# API Endpoints Overview

## Available Endpoints
```
GET /payments
GET /payments/:id
POST /payments
PUT /payments
DELETE /payments/:id
```

## Response format
Response follows non-strict HAL and Hateoas specs. Response format is as follows:
```JSON
{
    "data": {
      "id": 1,
      "amount": 12.30,
      "currency": "EUR"
    },
    "_metadata": {...},
    "_links": {...}
}
```
Entity itself is very simplistic.



