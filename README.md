Sample Spring Boot Application with one resource.

## Technical

### Stack
* Java 10+
* Spring Boot 2.0.2

### API documentation

* Generated swagger docs - http://localhost:8080/swagger-ui.html
* More info - [API design overview](APIDesign.md)
