package lt.vaidotas.sm.payments.components.common;

public class ValidationException extends RuntimeException {

    private String field;
    private String message;

    public ValidationException(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
