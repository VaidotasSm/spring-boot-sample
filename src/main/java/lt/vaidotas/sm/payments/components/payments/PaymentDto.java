package lt.vaidotas.sm.payments.components.payments;

import java.math.BigDecimal;

public class PaymentDto {
    private Long id;
    private BigDecimal amount;
    private String currency;

    public PaymentDto() {
    }

    public PaymentDto(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public PaymentDto(long id, BigDecimal amount, String currency) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
