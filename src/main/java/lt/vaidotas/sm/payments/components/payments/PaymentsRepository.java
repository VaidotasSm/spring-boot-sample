package lt.vaidotas.sm.payments.components.payments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface PaymentsRepository extends JpaRepository<PaymentEntity, Long> {
}
