package lt.vaidotas.sm.payments.components.payments;

import lt.vaidotas.sm.payments.components.common.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class PaymentsService {

    private final PaymentsRepository paymentsRepository;

    @Autowired
    public PaymentsService(PaymentsRepository paymentsRepository) {
        this.paymentsRepository = paymentsRepository;
    }

    @Transactional
    public Page<PaymentDto> findPayments(PageRequest pageRequest) {
        var paginator = paymentsRepository.findAll(pageRequest);
        return paginator.map(this::toDto);
    }

    @Transactional
    public Optional<PaymentDto> findPayment(long paymentId) {
        return paymentsRepository.findById(paymentId)
            .map(this::toDto);
    }

    @Transactional
    public Optional<Boolean> deletePayment(long paymentId) {
        return paymentsRepository.findById(paymentId).map((payment) -> {
            paymentsRepository.delete(payment);
            return true;
        });
    }

    @Transactional
    public PaymentDto createPayment(PaymentDto newPayment) {
        var entity = fromDto(newPayment);
        if(entity.getId() != null) {
            throw new ValidationException("id", "must be empty");
        }

        var created = paymentsRepository.save(entity);
        return this.toDto(created);
    }

    @Transactional
    public Optional<PaymentDto> updatePayment(Long paymentId, PaymentDto updated) {
        return paymentsRepository.findById(paymentId).map((original) -> {
            original.setAmount(updated.getAmount());
            original.setCurrency(updated.getCurrency());
            return toDto(paymentsRepository.save(original));
        });
    }


    private PaymentDto toDto(PaymentEntity e) {
        return new PaymentDto(e.getId(), e.getAmount(), e.getCurrency());
    }

    private PaymentEntity fromDto(PaymentDto e) {
        return new PaymentEntity(e.getId(), e.getAmount(), e.getCurrency());
    }

}
