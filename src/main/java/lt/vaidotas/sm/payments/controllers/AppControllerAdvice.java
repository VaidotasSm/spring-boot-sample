package lt.vaidotas.sm.payments.controllers;

import lt.vaidotas.sm.payments.controllers.http.ApiException;
import lt.vaidotas.sm.payments.controllers.http.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class AppControllerAdvice {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> mapToHttpStatus(Exception ex) {
        if (ex instanceof ApiException) {
            ApiException apiEx = (ApiException) ex;
            return new ResponseEntity<>(new ErrorResponse(apiEx.getMessage()), apiEx.getStatus());
        }

        if (ex instanceof NoHandlerFoundException) {
            return new ResponseEntity<>(new ErrorResponse("Not found"), HttpStatus.NOT_FOUND);
        }

        log.error("Error occured", ex);
        return new ResponseEntity<>(
            new ErrorResponse("Something went wrong"),
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

}
