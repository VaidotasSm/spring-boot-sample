package lt.vaidotas.sm.payments.controllers.http;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.HashMap;
import java.util.Map;

public class ResponseResource<T> extends ResourceSupport {
    @JsonProperty("data")
    private T data;

    @JsonProperty("_metadata")
    private Map<String, Object> metadata = new HashMap<>();

    public ResponseResource(T data) {
        this.data = data;
    }

    @JsonCreator
    public ResponseResource(@JsonProperty("data") T data, @JsonProperty("_metadata") Map<String, Object> metadata) {
        this.data = data;
        this.metadata = metadata;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public void addMetadata(String name, Object value) {
        this.metadata.put(name, value);
    }
}
