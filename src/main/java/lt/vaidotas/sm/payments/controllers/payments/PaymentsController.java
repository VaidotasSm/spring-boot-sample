package lt.vaidotas.sm.payments.controllers.payments;

import lt.vaidotas.sm.payments.components.payments.PaymentDto;
import lt.vaidotas.sm.payments.components.payments.PaymentsService;
import lt.vaidotas.sm.payments.controllers.http.ApiException;
import lt.vaidotas.sm.payments.controllers.http.ResponseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/payments")
public class PaymentsController {

    private static final int DEFAULT_PAGE_SIZE = 10;

    private final PaymentsService paymentsService;

    @Autowired
    public PaymentsController(PaymentsService paymentsService) {
        this.paymentsService = paymentsService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    ResponseResource<List<PaymentDto>> getPayments(
        @RequestParam(name = "page", defaultValue = "0") Integer page,
        @RequestParam(name = "page_size", defaultValue = DEFAULT_PAGE_SIZE + "") Integer pageSize) {

        var paginator = paymentsService.findPayments(PageRequest.of(page, pageSize));
        var resource = new ResponseResource<>(paginator.getContent());
        resource.addMetadata("pagination", Map.of(
            "page", page,
            "size", paginator.getSize(),
            "total_pages", paginator.getTotalPages()
        ));

        if (page < paginator.getTotalPages() - 1) {
            resource.add(linkTo(methodOn(PaymentsController.class).getPayments(page + 1, pageSize)).withRel("next"));
        }
        if (page > 0 && page < paginator.getTotalPages()) {
            resource.add(linkTo(methodOn(PaymentsController.class).getPayments(page - 1, pageSize)).withRel("previous"));
        } else if (page > 0 && page >= paginator.getTotalPages()) {
            resource.add(linkTo(methodOn(PaymentsController.class).getPayments(paginator.getTotalPages() - 1, pageSize)).withRel("previous"));
        }
        return resource;
    }

    @RequestMapping(value = "/{paymentId}", method = RequestMethod.GET)
    ResponseResource<PaymentDto> getPayment(@PathVariable Long paymentId) {
        var payment = paymentsService.findPayment(paymentId)
            .orElseThrow(ApiException::notFound);
        return paymentResource(payment);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    ResponseEntity<ResponseResource<PaymentDto>> createPayment(@RequestBody PaymentDto payment) {
        var created = paymentsService.createPayment(payment);
        URI uri = linkTo(PaymentsController.class).slash(created.getId()).toUri();
        return ResponseEntity.created(uri).body(paymentResource(created));
    }

    @RequestMapping(value = "/{paymentId}", method = RequestMethod.DELETE)
    ResponseEntity deletePayment(@PathVariable Long paymentId) {
        paymentsService.deletePayment(paymentId).orElseThrow(ApiException::notFound);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{paymentId}", method = RequestMethod.PUT)
    ResponseResource<PaymentDto> updatePayment(@PathVariable Long paymentId, @RequestBody PaymentDto payment) {
        var updated = paymentsService.updatePayment(paymentId, payment)
            .orElseThrow(ApiException::notFound);
        return paymentResource(updated);
    }

    private ResponseResource<PaymentDto> paymentResource(PaymentDto payment) {
        var resource = new ResponseResource<>(payment);
        resource.add(linkTo(methodOn(PaymentsController.class).getPayment(payment.getId())).withSelfRel());
        resource.add(linkTo(methodOn(PaymentsController.class).getPayments(0, DEFAULT_PAGE_SIZE)).withRel("up"));
        resource.add(linkTo(methodOn(PaymentsController.class).deletePayment(payment.getId())).withRel("delete"));
        return resource;
    }

}
