package lt.vaidotas.sm.payments;

import com.jayway.restassured.http.ContentType;
import lt.vaidotas.sm.payments.components.payments.PaymentDto;
import lt.vaidotas.sm.payments.components.payments.PaymentsService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentsTest {
    @LocalServerPort
    private int port;

    @Autowired
    private PaymentsService paymentService;

    @Test
    public void shouldReturnPayments() throws Exception {
        when().get(getUrl("/payments")).prettyPeek()
            .then()
            .statusCode(200)
            .body(
                "data[0].id", equalTo(1),
                "data[1].id", equalTo(2),
                "_metadata.pagination.page", equalTo(0),
                "_metadata.pagination.size", equalTo(10),
                "_metadata.pagination.total_pages", equalTo(2)
            );
    }

    @Test
    public void shouldReturnOnePayment() throws Exception {
        when().get(getUrl("/payments/1")).prettyPeek()
            .then()
            .statusCode(200)
            .body(
                "data.id", equalTo(1),
                "_links.self.href", containsString("/payments/1"),
                "_links.up.href", containsString("/payments"),
                "_links.delete.href", containsString("/payments/1")
            );
    }

    @Test
    public void shouldReturn404WhenNotPresent() throws Exception {
        when().get(getUrl("/payments/12345")).prettyPeek()
            .then()
            .statusCode(404)
            .body(
                "message", equalTo("Record not found")
            );
    }

    @Test
    public void shouldCreatePayment() throws Exception {
        JSONObject jsonObj = new JSONObject()
            .put("amount", 10.11)
            .put("currency", "USD");
        given()
            .contentType(ContentType.JSON)
            .body(jsonObj.toString())
            .when().post(getUrl("/payments")).prettyPeek()
            .then()
            .statusCode(201)
            .header("Location", containsString("/payments/"))
            .body(
                "data.id", notNullValue(),
                "data.amount", equalTo(10.11f),
                "data.currency", equalTo("USD"),
                "_links.self.href", containsString("/payments/"),
                "_links.up.href", containsString("/payments"),
                "_links.delete.href", containsString("/payments/")
            );
    }

    @Test
    public void shouldUpdatePayment() throws Exception {
        var created = paymentService.createPayment(new PaymentDto(BigDecimal.valueOf(11.13), "GBP"));

        JSONObject jsonObj = new JSONObject()
            .put("amount", 11.15)
            .put("currency", "EUR");
        given()
            .contentType(ContentType.JSON)
            .body(jsonObj.toString())
            .when().put(getUrl("/payments/" + created.getId())).prettyPeek()
            .then()
            .statusCode(200)
            .body(
                "data.amount", equalTo(11.15f),
                "data.currency", equalTo("EUR"),
                "_links.self.href", containsString("/payments/" + created.getId()),
                "_links.up.href", containsString("/payments"),
                "_links.delete.href", containsString("/payments/" + created.getId())
            );
    }

    @Test
    public void shouldDeletePayment() throws Exception {
        var created = paymentService.createPayment(new PaymentDto(BigDecimal.valueOf(11.13), "GBP"));

        when().delete(getUrl("/payments/" + created.getId())).prettyPeek()
            .then()
            .statusCode(204);

        var after = paymentService.findPayment(created.getId());
        assertThat(after.isPresent(), is(false));
    }

    private String getUrl(String uri) {
        return "http://localhost:" + port + uri;
    }

}
